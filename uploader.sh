#!/usr/bin/env bash
set -euo pipefail

echo "Checking upload queue dir..."
export UPLOAD_QUEUE="${UPLOAD_QUEUE:-/data/upload-queue/}"
mkdir -p "${UPLOAD_QUEUE}"

cd "${UPLOAD_QUEUE}"
while [ ! -f ../STOP ]
do
	echo "Starting new loop..."
	for pack in */; do
		echo "Trying claim ${pack}..."
		set +e
		flock --verbose -x --conflict-exit-code 234 --nonblock "$pack" /upload-single.sh "$pack"
		lock_rc="${?}"
		set -e
		if [ "$lock_rc" -ne 0 ]; then
			if [ "$lock_rc" -eq 234 ]; then
				echo "Could not claim pack!"
			else
				echo "Error return: ${lock_rc}."
				exit "${lock_rc}"
			fi
		fi
		if [ -f ../STOP ]; then
			exit 0
		fi
	done
	sleep 30
done
