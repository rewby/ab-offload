FROM debian:bullseye

# Install tini
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

# Install ia cli tool.
RUN apt-get update && apt-get dist-upgrade -y && apt-get install -y \
      build-essential dpkg-dev devscripts cmake git python3 python3-dev python3-pip \
      && rm -rf /var/lib/apt/lists/*
RUN pip3 install internetarchive

# Create data mount
RUN mkdir -p /data
RUN chown nobody:nogroup /data

WORKDIR /

# Include files
COPY mover.sh /mover.sh
COPY entrypoint.sh /entrypoint.sh
COPY uploader.sh /uploader.sh
COPY upload-single.sh /upload-single.sh

ENTRYPOINT [ "/tini", "--", "/entrypoint.sh" ]
