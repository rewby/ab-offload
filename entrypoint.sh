#!/bin/bash
set -exuo pipefail

# https://stackoverflow.com/a/2173421
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

export SHARED_WARCS_DIR="${SHARED_WARCS_DIR:-/data/}"
export INCOMING="${INCOMING:-${SHARED_WARCS_DIR}/incoming/}"
export UPLOAD_QUEUE="${UPLOAD_QUEUE:-${SHARED_WARCS_DIR}/upload-queue/}"
export MOVER_WORKING_DIR="${MOVER_WORKING_DIR:-${SHARED_WARCS_DIR}/mover-work/}"

function makedir() {
	mkdir -pv "$1"
	chown nobody:nogroup "$1"
}

makedir "${INCOMING}"
makedir "${UPLOAD_QUEUE}"
makedir "${MOVER_WORKING_DIR}"

case "$1" in
	"mover")
		setpriv --reuid=nobody --regid=nogroup --init-groups --inh-caps=-all /mover.sh
		;;
	"uploader")
		setpriv --reuid=nobody --regid=nogroup --init-groups --inh-caps=-all /uploader.sh
		;;
esac

