#!/bin/bash
set -euo pipefail

export IA_CONFIG_FILE="${IA_CONFIG_FILE:-/ia.ini}"

if [[ ! -d "$1" ]] ; then
	echo "Target directory $1 does not exist!"
	exit
fi

set -x

cd "$1"
export upload="$(basename "$(pwd)")"
export CUR_SIZE=$( du -B1 -s . | grep -oE "^[0-9]+" )
export YEAR=$(date +%Y)
ia upload "archiveteam_archivebot_go_${upload}" * \
        --size-hint="${CUR_SIZE}" \
	--sleep=120 \
	--retries=100 \
	--metadata="noarchivetorrent:true" \
	--metadata="size-hint:${CUR_SIZE}" \
	--metadata="mediatype:web" \
	--metadata="description:ArchiveBot is an Archive Team service to quickly grab smaller at-risk or critical sites to bring copies into the Internet Archive Wayback machine." \
	--metadata="title:Archiveteam: Archivebot GO Pack ${upload}" \
	--metadata="collection:archivebot" \
	--metadata="creator:Archive Team" \
	--metadata="date:${YEAR}" \
	--checksum \
	--verify \
	--delete

cd ..
rmdir "$upload"
